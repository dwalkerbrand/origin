<section class="w-full h-screen bg-primary_700">
    <div class="w-full h-full lg:bg-slider_productos lg:bg-cover lg:bg-center flex justify-center items-center">
        <div class="bg-white p-8 w-2/3 rounded-lg">
            <div class="text-5xl font-bold">Conócenos</div>

            <p class="pt-6">
                A partir del año 2012, Enkador ingresa al mercado de producción de resinas plásticas con el reciclaje de
                PET post consumo, con la marca Recypet®. La incursión de la empresa en este segmento se generó debido a
                la conciencia de protección medioambiental de sus directivos; la misma ha ido evolucionando y
                ajustándose a las demandas de los consumidores que esperan productos sociales y ambientalmente
                responsables: por eso nos llamamos Origin.

            </p>
            <p class="text-center pt-6">
                Nuestra planta de reciclaje actualmente opera con equipos italianos (Sorema-Previero) para lograr la
                producción de escama lavada y erema.
            </p>
            <div class="mt-4">
                <p class="text-xl font-bold">Cifras: </p>

                <p>
                    <b>+1.400 personas</b><br>
                    participan en la recolección y acopio.<br>
                    <b>+1.5 millones de botellas</b><br>
                    diarias de ciudades, ríos, parques y playas del país son recolectadas.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="bg-light_grey py-20">
    <div class="container-origin grid grid-cols-2">
        <div class="">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/recoleccion-botellas.png" alt="Socio 1">
        </div>
        <div class="">
            <div class="text-5xl font-bold py-4">Nuestros productos</div>
            <div class="mb-3">
                <h3 class="font-bold text-2xl">Escama de rPET</h3>
                <p>
                    Obtenido a partir del reciclado de botellas de PET post-consumo.
                    <br>
                    <br>
                    Escama u hojuela molida y lavada en caliente óptimo para pelletizado, laminado u otros. Puede ser
                    utilizado para inyección de botellas.
                </p>
            </div>

            <div class="mb-3">
                <h3 class="font-bold text-2xl">Resina de rPET</h3>
                <p>
                    Resina de Polietileno tereftalato post-consumo.
                    <br>
                    <br>
                    Producto pelletizado de alta pureza ideal para la fabricación de botellas y envases que contengan
                    alimentos y/o bebidas para el consumo humano.
                </p>
            </div>

            <div class="mb-3">
                <h3 class="font-bold text-2xl">Preformas de rPET</h3>
                <p>
                    Tubo de Polietileno tereftalato post-consumo utilizado para la fabricación de botellas mediante el
                    proceso de inyección soplado.
                </p>
            </div>

            <div class="mb-3">
                <h3 class="font-bold text-2xl">Botellas de rPET</h3>
                <p>
                    Botellas de Polietileno tereftalato post-consumo hasta 100% de material reciclado. Eco-envase
                    diseñado en función de la necesidad del cliente, grado alimenticio.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="bg-white">
    <div class="container-origin py-20">
        <div class="text-5xl font-bold">Certificaciones</div>
        <p>
            Contamos con certificaciones a nivel internacional que avalan los procesos que llevamos a cabo en la
            elaboración de nuevos productos con R-PET en Origin.
        </p>

        <div class="flex justify-between items-center pt-8 space-x-12">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/certificado-1.png" alt="Socio 1">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/certificado-2.png" alt="Socio 1">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/certificado-3.png" alt="Socio 1">
        </div>
        <div class="flex justify-center items-center pb-8 space-x-12">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/certificado-4.png" alt="Socio 1">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/certificado-5.png" alt="Socio 1">
        </div>
    </div>
</section>